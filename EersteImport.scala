import org.apache.commons.io.IOUtils
import java.net.URL
import java.nio.charset.Charset

val realdata = sc.textFile("/root/application.txt")

case class testClass(date: String, time: String, level: String, unknown1: String, unknownConsumer: String, unknownConsumer2: String, vloer: String, tegel: String, msg: String, bool1: String, bool2: String, bool3: String, bool4: String, bool5: String, bool6: String, bool7: String, bool8: String, batchsize: String, troepje1: String, troepje2: String)

//val testje = realdata.filter(line => line.contains("INFO"))
val mapData = realdata.map(s => s.split(" ")).filter(line => line.contains("INFO")).map(
    s => testClass(s(0),
        s(1),
        s(2),
        s(3),
        s(4), 
        s(5),
        s(6),
        s(7),
        s(8),
        s(9),
        s(10),
        s(11),
        s(12),
        s(13),
        s(14),
        s(15),
        s(16),
        s(17),
        s(18),
        s(19)
        )
    ).toDF()
    mapData.registerTempTable("prayer")
